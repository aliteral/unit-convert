#include <stdio.h>
#include <math.h>
#include <string.h>
#include "option.h"

// variable declaration

const double siMeter = 1;
const double siInches = 39.37;
const double cm = siMeter / 100;
//const float km = siMeter * 100;
const double mm = siMeter / 1000;
double selectedNbr;
//char optCM[3] = "cm";
//char optIN[3] = "in";
//char optionKM[3] = "km";
//char optionMM[3] = "mm";

// function declaration
//float convert();

int main(int argc, char *argv[]) 
{
	// check for argument count
	if (argc < 2)
	{
		printf("Not enough arguments!\n");
		return -1;
	}
	
	// unit selection block
	char input[3];
	char output[3];	

	printf("Select an input unit\n");
	scanf("%s", input);
	printf("Select an output unit\n");
	scanf("%s", output);
	printf("The equivalence is set to: %s to %s\n", input, output);

	// conversion block

	if (strcmp(input, optCM) == 0 && strcmp(output, optIN) == 0)
	{
		printf("Select a number:\n");
		scanf("%lf", &selectedNbr);
		printf("Your number is %lf\n", selectedNbr);
		double conversion = ((cm * siInches) * selectedNbr);
		printf("Conversion equals to %lf%s\n", conversion, output);
	}
	else
	{
		printf("Terms do not match\n");
	}

	return 0;

}


// TODO:
//
// Make the program know which type of units is it working with
// Use proper control flow to determine outcomes
// Replace basic conversion for a more refined/refactorized function
// Make conversion functions
